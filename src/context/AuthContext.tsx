import {createContext, useEffect, useReducer} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  LoginData,
  LoginResponse,
  RegisterData,
} from '../interfaces/appInterfaces';
import {AuthState, authReducer} from './AuthReducer';
import habitosJavaApi from '../api/habitosJavaApi';

type AuthContextProps = {
  errorMessage: string;
  token: string | null;
  email: string | null;
  status: 'checking' | 'authenticated' | 'non-authenticated';
  signUp: (registerData: RegisterData) => void;
  signIn: (loginData: LoginData) => void;
  logOut: () => void;
  removeError: () => void;
};

const authInitialState: AuthState = {
  status: 'checking',
  token: null,
  email: null,
  errorMessage: '',
};

export const AuthContext = createContext({} as AuthContextProps);

export const AuthProvider = ({children}: any) => {
  const [state, dispatch] = useReducer(authReducer, authInitialState);

  useEffect(() => {
    if (state.status !== 'authenticated') {
      checkToken(children.email);
    }
  });

  const checkToken = async (email: string) => {
    const token = await AsyncStorage.getItem('token');

    if (!token) {
      return dispatch({type: 'notAuthenticated'});
    }

    const resp = await habitosJavaApi.get('/auth');

    if (resp.status !== 200) {
      return dispatch({type: 'notAuthenticated'});
    }

    // await AsyncStorage.setItem('token', resp.data.token);
    dispatch({
      type: 'signUp',
      payload: {token: resp.data.token, email: email},
    });
  };

  const signIn = async ({correo, password}: LoginData) => {
    try {
      const resp = await habitosJavaApi.post<LoginResponse>('/auth/login', {
        correo,
        password,
      });
      dispatch({
        type: 'signUp',
        payload: {token: resp.data.token, email: correo},
      });
      console.log(resp);

      await AsyncStorage.setItem('token', resp.data.token);
    } catch (error: any) {
      dispatch({
        type: 'addError',
        payload: error.response.data.msg || 'Información incorrecta',
      });
    }
  };
  const signUp = async ({nombre, correo, password}: RegisterData) => {
    try {
      const resp = await habitosJavaApi.post<LoginResponse>('/usuarios', {
        correo,
        password,
        nombre,
      });
      dispatch({
        type: 'signUp',
        payload: {token: resp.data.token, email: correo},
      });

      await AsyncStorage.setItem('token', resp.data.token);
    } catch (error: any) {
      dispatch({
        type: 'addError',
        payload: error.response.data.msg || 'El correo ya está registrado',
      });
    }
  };

  const logOut = async () => {
    await AsyncStorage.removeItem('token');
    dispatch({type: 'logout'});
  };

  const removeError = () => {
    dispatch({type: 'removeError'});
  };

  return (
    <AuthContext.Provider
      value={{
        ...state,
        signUp,
        signIn,
        logOut,
        removeError,
      }}>
      {children}
    </AuthContext.Provider>
  );
};
