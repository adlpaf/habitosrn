// import {Usuario} from '../interfaces/appInterfaces';

export interface AuthState {
  status: 'checking' | 'authenticated' | 'non-authenticated';
  token: string | null;
  errorMessage: string;
  email: string | null;
}

type AuthAction =
  | {type: 'signUp'; payload: {token: string; email: string}}
  | {type: 'addError'; payload: string}
  | {type: 'removeError'}
  | {type: 'notAuthenticated'}
  | {type: 'logout'};

export const authReducer = (
  state: AuthState,
  action: AuthAction,
): AuthState => {
  switch (action.type) {
    case 'addError':
      return {
        ...state,
        email: null,
        status: 'non-authenticated',
        token: null,
        errorMessage: action.payload,
      };
    case 'removeError':
      return {
        ...state,
        errorMessage: '',
      };
    case 'signUp':
      return {
        ...state,
        errorMessage: '',
        status: 'authenticated',
        token: action.payload.token,
        email: action.payload.email,
      };
    case 'notAuthenticated':
    case 'logout':
      return {
        ...state,
        status: 'non-authenticated',
        token: null,
        email: null,
      };
    default:
      return state;
  }
};
